CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
A plugin to combine Joomla! and Varnish on the Byte clustered platform.

In cooperation with Mediagrip Byte developed its own extension for Drupal. With 
this extension Drupal sites can run on the superfast Byte Varnish Cluster. The 
extension allows Drupal to automatically empty the cache for specific pages 
after changing an article in Drupal and cache the entire site should be emptied 
after clicking a special button.

 * For full documentation (dutch):
   https://www.byte.nl/wiki/Varnish_voor_Drupal

 * For a full description of the module, visit our sandbox:
   https://www.drupal.org/sandbox/bytebv/2424993

REQUIREMENTS
------------
This module requires the following modules:
 * Cache Expiration (https://www.drupal.org/project/expire)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * You can configure Varnish expiration times through the Drupal performance
   administration. 
 * You can configure cache expiration trough the Cache Expiration module.
